# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class project_task_type_action(models.Model):
    _name = "project.task.type.action"

    name = fields.Char(
        required=True,
    )
    duration = fields.Integer(
        required=True,
    )
