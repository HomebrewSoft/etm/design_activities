# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class project_task(models.Model):
    _inherit = "project.task"

    product_id = fields.Many2one(
        comodel_name="product.product",
    )

    task_type_id = fields.Many2one(
        comodel_name="project.task.type.action",
    )
