# -*- coding: utf-8 -*-
{
    "name": "Design Activities",
    "version": "13.0.0.1.0",
    "author": "HomebrewSoft",
    "website": "https://homebrewsoft.dev",
    "license": "LGPL-3",
    "depends": [
        "project",
    ],
    "data": [
        # security
        "security/ir.model.access.csv",
        # views
        "views/design_activities.xml",
        "views/project_task.xml",
    ],
}
